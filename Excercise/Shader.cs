﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using OpenTK.Graphics.OpenGL;

namespace OpenTKTemplate
{
 
    public class Shader : IDisposable
    {
        int Handle;
        int VertexHandle, FragmentHandle;
        public Shader(string vertexPath, string fragmentPath)
        {
            string vertexData, fragmentData;
            vertexData = File.ReadAllText(vertexPath, Encoding.UTF8);
            fragmentData = File.ReadAllText(fragmentPath, Encoding.UTF8);

            VertexHandle = GL.CreateShader(ShaderType.VertexShader);
            GL.ShaderSource(VertexHandle, vertexData);

            FragmentHandle = GL.CreateShader(ShaderType.FragmentShader);
            GL.ShaderSource(FragmentHandle, fragmentData);

            CompileShader(VertexHandle);
            CompileShader(FragmentHandle);

            Handle = GL.CreateProgram();
            GL.AttachShader(Handle, VertexHandle);
            GL.AttachShader(Handle, FragmentHandle);

            GL.LinkProgram(Handle);

            GL.DetachShader(Handle, VertexHandle);
            GL.DetachShader(Handle, FragmentHandle);

            GL.DeleteShader(VertexHandle);
            GL.DeleteShader(FragmentHandle);
        }

        private void CompileShader(int Handle)
        {
            GL.CompileShader(Handle);
            string infoLog = GL.GetShaderInfoLog(Handle);
            if (!string.IsNullOrEmpty(infoLog))
                Console.WriteLine(infoLog);

        }
       
        public void Use()
        {
            GL.UseProgram(Handle);
        }

        private bool isDisposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!isDisposed)
            {
                GL.DeleteProgram(Handle);
                isDisposed = true;
            }
        }

        ~Shader()
        {
            GL.DeleteProgram(Handle);
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
